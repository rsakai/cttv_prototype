class TextList{
	ArrayList<String> list;
	int title_height = 15;
	int item_height = 15;
	int scroll_bar_width = 15;


	int item_text_highlight = 60;
	int item_text_color = 120;
	int list_bg_color = 255;
	int list_bg_outline = 180;
	int scroll_area_color = 230;
	int scroll_color = 120;

	Rectangle whole_rect, scroll_bar, scroll_area;

	int list_index = 0; //current position of index
	int list_index_min = 0;
	int list_index_max;

	int list_item_count = 0;

	boolean isActive = false;
	boolean onDrag = false;
	int onDrag_pos = 0;
	float onPress_pos = 0; //relative position

	String hovering_item = "";
	String selected_item = "";

	TextList(Rectangle area){
		list = new ArrayList<String>();
		int scroll_x = area.x + area.width - scroll_bar_width;
		int scroll_y = area.y;
		int scroll_w = scroll_bar_width;
		int scroll_h = area.height;
		list_item_count = round((float)scroll_h/ (float)item_height);
		scroll_h =  list_item_count * item_height;
		
		this.whole_rect = area;
		scroll_area = new Rectangle(scroll_x, scroll_y, scroll_w, scroll_h);
		scroll_bar = new Rectangle(scroll_x, scroll_y, scroll_w, scroll_h);
	}

	void display(){
		rectMode(CORNER);
		// noStroke();
		// noFill();
		stroke(list_bg_outline);
		fill(list_bg_color);
		rect(whole_rect.x, whole_rect.y, whole_rect.width, whole_rect.height);
		//scroll scroll_area
		fill(scroll_area_color);
		rect(scroll_area.x, scroll_area.y, scroll_area.width, scroll_area.height);

		//scroll bar
		int total_item_count = list.size();
		// println("total item count = "+total_item_count);
		if(total_item_count > list_item_count){
			// int bar_height = round(scroll_area.height * (float)list_item_count/(float)total_item_count);
			int bar_y = round(map(list_index, 0, total_item_count, scroll_area.y, scroll_area.y+scroll_area.height));
			scroll_bar.setLocation(scroll_area.x, bar_y);
			noStroke();
			fill(scroll_color);	
			//rounded rect
			rect(scroll_bar.x, scroll_bar.y, scroll_bar.width, scroll_bar.height, 5, 5, 5, 5);
		}
		
		//draw items
		textAlign(LEFT, CENTER);
		float runningY = scroll_area.y + (float)item_height/2;
		float runningX = whole_rect.x;
		
		for(int i = list_index; i < list_index+list_item_count; i++){
			if(i < list.size()){
				fill(item_text_color);
				if(hovering_item.equals(list.get(i))){
					fill(item_text_highlight);
				}
				//when selected
				if(selected_item.equals(list.get(i))){
					fill(color_red);
				}
				text("  "+list.get(i), runningX, runningY);
				runningY+= item_height;
			}
		}
	}

	//call after adding all the items
	void update(){
		int item_count = list.size();
		list_index = 0;
		list_index_min = 0;
		if(item_count > list_item_count){
			list_index_max = item_count - list_item_count;
			int bar_height = round(scroll_area.height * (float)list_item_count/(float)item_count);
			bar_height = max(bar_height, 5);

			int bar_y = round(map(list_index, 0, item_count-1, scroll_area.y, scroll_area.y+scroll_area.height));
			scroll_bar = new Rectangle(scroll_area.x, bar_y, scroll_bar_width, bar_height);

		}else{
			list_index_max = 1;
		}
	}

	//update index based on registerd onDrag_pos in relation to m_y
	void updateList(int m_y){
		//determine tip of bar
		int tip_y = round(constrain(m_y - onPress_pos*scroll_bar.height, scroll_area.y, scroll_area.y+scroll_area.height- scroll_bar.height));
		
		//updeate index
		list_index = round(map(tip_y, scroll_area.y, scroll_area.y + scroll_area.height, 0, list.size()));
		list_index = constrain(list_index, 0, list.size()-1);
	}


	void addItem(String name){
		this.list.add(name);
	}

	void reset(){
		list = new ArrayList<String>();
		hovering_item = "";
		selected_item = "";
	}

	boolean checkMouseOver(int mx, int my){
		int n =  floor((my - whole_rect.y)/item_height) + list_index;
		if(n < list.size()){
			//node list
			String label = list.get(n);
			// println("debug label = "+label);
			hovering_item = label;
			return true;
		}else{
			hovering_item = "";
		}
		return false;
	}

}