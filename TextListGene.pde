class TextListGene extends TextList{
	TextListGene(Rectangle area){
		super(area);
	}

	void display(){
		rectMode(CORNER);
		stroke(list_bg_outline);
		fill(list_bg_color);
		rect(whole_rect.x, whole_rect.y, whole_rect.width, whole_rect.height);
		//scroll scroll_area
		fill(scroll_area_color);
		rect(scroll_area.x, scroll_area.y, scroll_area.width, scroll_area.height);

		//scroll bar
		int total_item_count = list.size();
		// println("total item count = "+total_item_count);
		if(total_item_count > list_item_count){
			// int bar_height = round(scroll_area.height * (float)list_item_count/(float)total_item_count);
			int bar_y = round(map(list_index, 0, total_item_count, scroll_area.y, scroll_area.y+scroll_area.height));
			scroll_bar.setLocation(scroll_area.x, bar_y);
			noStroke();
			fill(scroll_color);	
			//rounded rect
			rect(scroll_bar.x, scroll_bar.y, scroll_bar.width, scroll_bar.height, 5, 5, 5, 5);
		}
		
		//draw items
		textAlign(LEFT, CENTER);
		float runningY = scroll_area.y + (float)item_height/2;
		float runningX = whole_rect.x;
		
		strokeCap(SQUARE);
		for(int i = list_index; i < list_index+list_item_count; i++){
			if(i < list.size()){
				fill(item_text_color);
				if(hovering_item.equals(list.get(i))){
					fill(item_text_highlight);
				}
				//when selected
				if(selected_item.equals(list.get(i))){
					fill(color_red);
				}
				text("  "+list.get(i), runningX, runningY);

				//draw Link type
				Gene g = display_gene_array.get(i);
				float content_x = scroll_area.x - _margin;
				for(Link l : g.links){
					strokeWeight(2);
					stroke(color_qualitative[source_array.indexOf(l.source)]);
					line(content_x, runningY, content_x, runningY+item_height);
					content_x -= 3;
				}
				runningY+= item_height;
			}
		}
		strokeWeight(1);
	}
}