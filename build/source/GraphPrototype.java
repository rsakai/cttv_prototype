import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.awt.*; 
import java.util.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class GraphPrototype extends PApplet {





int _margin = 10;
int _top_margin = _margin*2;
int _bottom_margin = _margin *2;
int _left_margin = _margin*5;
int _right_margin = _margin*5;
int _stage_width, _stage_height;
int _window_bar_height = 50;

int list_width = _margin *20;
int list_left_width = _margin*12;
int list_right_width = _margin*30;

int top_interface = _margin*3;
Rectangle left_list_rect, right_list_rect;
Rectangle plot_rect;

TextListGene source_list;
TextList target_list;


HashMap<String, Gene> gene_map;
ArrayList<Gene> gene_array;

HashMap<String, Disease> disease_map;
ArrayList<Disease> disease_array;

HashMap<String, DiseaseGroup> disease_group_map;
ArrayList<DiseaseGroup> disease_group_array;

HashSet<String> source_set;
ArrayList<String> source_array;

ArrayList<Link> link_array;

//array for display object
ArrayList<Gene> display_gene_array;
ArrayList<Disease> display_disease_array;
ArrayList<Link> display_link_array;


PFont font;
PFont font2;

//optimize cpu
int draw_counter = 0;
int draw_max = 30; //1 seconds = draw 30 times before it stops


String selected_gene ="";
String selected_disease ="";

int color_red = color(228,26,28); //red
int color_green = color(77,175,74); //green
int color_orange = color(225,127,0); //orange
int color_blue = color(55,126,184); //blue
int color_gray = color(120);// gray

//categorical color =  8 colors
// int[] color_qualitative = {color(27,158,119), 
// 	color(217,95,2), color(117,112,179), 
// 	color(231,41,138), color(102,166,30), 
// 	color(230,171,2), color(166,118,29), 
// 	color(102,102,102)};

int[] color_qualitative = {color(27,158,119),
	color(217,95,2),color(117,112,179),
	color(231,41,138),color(102,166,30),color(230,171,2)};


//design index
int design_index = 3;
String[] design_labels = {"1", "2", "3", "4", "5"};
// 3 flower_2
// 4 graph
// 5 nightingale 
Rectangle design_btn_area;
Rectangle[] design_btn_rects;

boolean isUpdating = false;


public void setup(){
	load_data();

	//debug
	for(Gene g: gene_array){
		if(g.links.size()>1){
			// println("debug:"+g.name+ " has "+g.links.size()+ " links");
		}
	}
	for(Disease d: disease_array){
		if(d.links.size() >1){
			// println("debug:"+d.links.size()+ " : "+d.name);
		}
	}
	for(DiseaseGroup dg: disease_group_array){
		// println("debug:"+dg.name+" size=" + dg.array.size());
	}

	println("debug:source:"+source_set.size());
	println("\t"+source_set);

	//make an array to get index of data source
	source_array = new ArrayList<String>();
	source_array.addAll(source_set);

	//debug count source types
	int[] counter = new int[source_array.size()];
	for(Link l: link_array){
		int index = source_array.indexOf(l.source);
		counter[index]++;
	}
	println("\t"+Arrays.toString(counter));



	//font
	font = createFont("Supernatural1001", 10);
	font2 = createFont("DroidSansMono", 20);
	textFont(font);

	//setup display
	setup_display();

	//update display array
	update_display_array();

	//setup list
	update_list();

}

public void load_data(){
	// JSONArray datat = loadJSONArray("genes2diseases.json");
	JSONObject data = loadJSONObject("genes2diseases.json");

	//find all keys
	HashSet keys = new HashSet(data.keys());
	for(Object obj:keys){
		String key = (String)obj;
		println("debug: key = "+ key);
	}

	JSONArray links = data.getJSONArray("links");
	JSONArray gene_nodes = data.getJSONArray("gene_nodes");
	JSONArray disease_nodes = data.getJSONArray("disease_nodes");

	println("debug: "+links.size()+" links");
	println("\t"+links.getJSONObject(0).keys());
	println("debug: "+gene_nodes.size()+" genes");
	println("\t"+gene_nodes.getJSONObject(0).keys());
	println("debug: "+disease_nodes.size()+" diseases");
	println("\t"+disease_nodes.getJSONObject(0).keys());



	// print(links.toString());
	// println(gene_nodes.toString());
	// println(disease_nodes.toString());


	//parse genes
	gene_map = new HashMap<String, Gene>();
	gene_array = new ArrayList<Gene>();
	for (int i = 0; i < gene_nodes.size(); i++) {
		JSONObject obj = gene_nodes.getJSONObject(i);
		String name = obj.getString("name");
		int group = obj.getInt("group");
		String acc = obj.getString("acc");
		Gene gene = new Gene(name, acc, group, i);
		gene_array.add(gene);
		gene_map.put(name, gene);
		// println(obj);
		// println(i+"\t"+name);
	}
	println("debug: total number of genes = "+gene_array.size());


	//parse diseases
	disease_map = new HashMap<String, Disease>();
	disease_array = new ArrayList<Disease>();
	disease_group_map = new HashMap<String, DiseaseGroup>();
	disease_group_array = new ArrayList<DiseaseGroup>();
	for(int i = 0; i < disease_nodes.size(); i++){
		JSONObject obj = disease_nodes.getJSONObject(i);
		String url = obj.getString("id");
		String name = obj.getString("name");
		String top_level = obj.getString("top_level");
		String type = obj.getString("type");
		int group = obj.getInt("group");
		String[] split = split(url, "/");
		String id = split[split.length-1];

		Disease d = new Disease(url, name, top_level, type, group, i);
		disease_array.add(d);
		disease_map.put(name, d);
		// println(obj);

		//DiseaseGroup
		DiseaseGroup dg = (DiseaseGroup)disease_group_map.get(top_level);
		if(dg == null){
			dg = new DiseaseGroup(top_level);
			disease_group_map.put(top_level, dg);
			disease_group_array.add(dg);
		}
		dg.array.add(d);
	}

	//parse links
	source_set = new HashSet<String>();
	link_array = new ArrayList<Link>();
	for(int i = 0; i < links.size(); i++){
		JSONObject obj = links.getJSONObject(i);
		String datasources = obj.getString("datasources");

		//remove []''
		datasources = datasources.replace("[", "").replace("]", "").replace("'", "");
		String[] sources = split(datasources, ",");
		if(sources.length >1){
			println("Error: more than one source");
		}

		int value = obj.getInt("value");
		int source_index = obj.getInt("source");
		int target_index = obj.getInt("target");
		Gene gene = gene_array.get(source_index);
		Disease disease = disease_array.get(target_index);
		
		Link link = new Link(gene, disease, sources[0], value);
		//save link in Gene and Disease obj
		gene.links.add(link);
		disease.links.add(link);
		link_array.add(link);

		source_set.add(link.source);

		// println(datasources+" gene:"+gene.name+"  disease:"+disease.name);
		// JSONObject sources = obj.getJSONObject("datasources");
		// println("debug: source ="+sources.size());
		// println(obj);
	}
}

public void setup_display(){
	//setup display
	_stage_width = displayWidth;
	_stage_height = displayHeight - _window_bar_height;
	size(_stage_width, _stage_height);

	left_list_rect = new Rectangle(0, top_interface, list_left_width, _stage_height - top_interface);
	right_list_rect = new Rectangle(_stage_width-list_right_width, top_interface, list_right_width, _stage_height -  top_interface);
	plot_rect = new Rectangle(list_left_width, top_interface, _stage_width-(list_left_width+list_right_width), _stage_height - top_interface);

	//textlist
	source_list = new TextListGene(left_list_rect);
	target_list = new TextList(right_list_rect);

	//design btns
	design_btn_area = new Rectangle(plot_rect.x, 0, plot_rect.width, top_interface);
	design_btn_rects = new Rectangle[design_labels.length];
	int runningX = design_btn_area.x;
	int runningY = design_btn_area.y;
	int btn_size = top_interface;
	for(int i= 0; i< design_labels.length; i++){
		design_btn_rects[i] = new Rectangle(runningX, runningY, btn_size, btn_size);
		runningX += btn_size;
	}

}	

//update the array for display based on selection
public void update_display_array(){
	// println("debug:update_display_array()");
	isUpdating = true;
	if(display_gene_array == null){
		display_gene_array = new ArrayList<Gene>();
		display_disease_array = new ArrayList<Disease>();
		display_link_array = new ArrayList<Link>();
	}
	display_gene_array.clear();
	display_disease_array.clear();
	display_link_array.clear();

	//if nothing is selected
	if(selected_gene.equals("") && selected_disease.equals("")){
		// println("\tboth NOT selected: gene="+selected_gene+"   disease="+selected_disease);
		display_gene_array.addAll(gene_array);
		display_disease_array.addAll(disease_array);
		//sort
		Collections.sort(display_gene_array, new GeneAlphabetComparator());
		Collections.sort(display_disease_array, new DiseaseAlphabetComparator());
	}else if(!selected_gene.equals("") && !selected_disease.equals("")){
		//both selected
		// println("\tboth selected: gene="+selected_gene+"   disease="+selected_disease);
		//selected gene
		Gene g = (Gene)gene_map.get(selected_gene);
		Disease d = (Disease)disease_map.get(selected_disease);
		if(g == null){
			println("Error:"+selected_gene+" is not found");
		}else{
			//find link
			if(d == null){
				println("Error:"+selected_disease+" is not found");
			}else{
				//find link
				display_link_array.addAll(g.getLinks(d));
				display_gene_array.add(g);
				display_disease_array.add(d);
				// println("number of links found:"+display_link_array.size());
			}
		}
	}else if(!selected_gene.equals("") && selected_disease.equals("")){
		//source/gene is selected
		// println("\tonly source is selected:"+selected_gene);
		Gene g = (Gene)gene_map.get(selected_gene);
		display_gene_array.add(g);
		//get array of Disease
		display_disease_array.addAll(g.getDiseaseArray());
		Collections.sort(display_disease_array, new DiseaseAlphabetComparator());

	}else if(selected_gene.equals("") && !selected_disease.equals("")){
		//target/disease is selected
		// println("\tonly target is selected:"+selected_disease);
		Disease d = (Disease)disease_map.get(selected_disease);
		display_disease_array.add(d);
		
		//get array of Gene
		// println("debug: number of links:"+d.links.size());
		display_gene_array.addAll(d.getGeneArray());
		Collections.sort(display_gene_array, new GeneAlphabetComparator());
	}

	update_list();

	isUpdating = false;
}

public void update_list(){
	// println("debug:update_list()");
	source_list.reset();
	target_list.reset();

	for(Gene g: display_gene_array){
		source_list.addItem(g.name);
	}
	for(Disease d: display_disease_array){
		target_list.addItem(d.name);
	}

	source_list.update();
	target_list.update();
}



public void draw(){
	background(255);
	stroke(120);
	fill(255);
	// rect(left_list_rect.x, left_list_rect.y, left_list_rect.width, left_list_rect.height);
	// rect(right_list_rect.x, right_list_rect.y, right_list_rect.width, right_list_rect.height);
	rect(plot_rect.x, plot_rect.y, plot_rect.width, plot_rect.height);


	source_list.display();
	target_list.display();

	draw_legend();

	//drawing main plot
	draw_plot();

	//draw btns
	draw_btns();

	if(draw_counter > draw_max){
		println("stop looping ---------");
		noLoop();
		draw_counter = 0;
	}else{
		draw_counter ++;
	}
}


public void draw_legend(){
	int runningX = plot_rect.x + _margin;
	int runningY = plot_rect.y + plot_rect.height - _bottom_margin - (source_array.size()*_margin);
	for(int i = 0; i < source_array.size(); i++){
		strokeWeight(2);
		stroke(color_qualitative[i]);
		line(runningX, runningY, runningX+_margin, runningY);

		fill(120);
		textAlign(LEFT, CENTER);
		text(source_array.get(i), runningX+_margin+2, runningY);
		runningY += _margin;
	}
	strokeWeight(1);
}

//drawing the main view
public void draw_plot(){
	if(design_index == 0 && !isUpdating){
		plot_0();
	}else if(design_index == 1 && !isUpdating){
		plot_1();
	}else if(design_index == 2){
		// plot_2_backup();
		plot_2();
	}else if(design_index == 3){
		plot_3();
	}else if(design_index == 4){
		plot_4();
	}

	textFont(font);
}

public void plot_0(){
	if(!selected_gene.equals("") || !selected_disease.equals("")){
		int gap = _margin*2;
		int max_height = _margin * 15;

		int g_count = display_gene_array.size();
		int d_count = display_disease_array.size();
		int source_count = source_array.size();
		int max_count = max(g_count, d_count);

		//determine small multiple size
		int sm_height = (plot_rect.height - _top_margin - _bottom_margin - gap*(max_count+1))/max_count;
		if(sm_height > max_height){
			sm_height = max_height;
			gap = (plot_rect.height - _top_margin - _bottom_margin - sm_height*max_count)/(max_count+1);
		}
		int sm_width = round(plot_rect.width * 0.4f);
		int label_width = round(plot_rect.width * 0.3f);

		Rectangle[] rects = new Rectangle[max_count];
		Gene[] genes = new Gene[max_count];
		Disease[] diseases = new Disease[max_count];

		int runningX = plot_rect.x + label_width;
		int runningY = plot_rect.y + gap;
		rectMode(CORNER);
		for(int i = 0; i < max_count; i++){
			//rectangle
			rects[i] = new Rectangle(runningX, runningY, sm_width, sm_height);
			//
			if(g_count == 1){
				genes[i] = display_gene_array.get(0);
			}else{
				if(i < display_gene_array.size()){
					genes[i] = display_gene_array.get(i);
				}
			}
			if(d_count == 1){
				diseases[i] = display_disease_array.get(0);
			}else{
				if(i < display_disease_array.size()){
					diseases[i] = display_disease_array.get(i);
				}
			}
			runningY += sm_height + gap;
		}	
		//bar size
		float bar_gap_total = (sm_height*0.2f); //size of gap > size of bar height
		float bar_gap = bar_gap_total/(float)(source_count-1);
		float bar_height = (sm_height - bar_gap_total)/ (float)source_count;

		//draw small multiples
		int max_link_value = 20;
		for(int i = 0; i < max_count; i++){
			Rectangle r = rects[i];
			Gene g = genes[i];
			Disease d = diseases[i];
			//draw background
			float runningY2 = r.y;
			//per data source
			for(int j = 0; j < source_count; j++){
				//outline
				stroke(240);
				strokeWeight(1);
				noFill();
				rect(r.x, runningY2, r.width, bar_height);
				if(g!= null && d!= null){
					Link l = g.getLink(d, source_array.get(j));
					if(l == null){
						//dont draw
					}else{
						noStroke();
						fill(color_qualitative[j]);
						float center_x = (float)r.getCenterX();
						float count_width = map(l.value, 0, max_link_value, 0, r.width/2);
						rect(center_x-count_width, runningY2, count_width*2, bar_height);

						//draw value
						fill(255);
						textAlign(CENTER, BOTTOM);
						textFont(font);
						text(l.value, center_x, runningY2+bar_height);
					}						
				}
				runningY2 += bar_height + bar_gap;
			}
			//text
			textFont(font2);
			fill(0);
			textAlign(CENTER, CENTER);
			text(g.name, plot_rect.x, r.y, label_width, r.height);
			text(d.name, r.x+r.width, r.y, label_width, r.height);
		}
	}
}

public void plot_1(){
	if(!selected_gene.equals("") || !selected_disease.equals("")){
		int gap = _margin*2;
		int max_height = _margin * 20;

		int g_count = display_gene_array.size();
		int d_count = display_disease_array.size();
		int source_count = source_array.size();
		int max_count = max(g_count, d_count);

		//determine small multiple size
		int sm_height = (plot_rect.height - _top_margin - _bottom_margin - gap*(max_count+1))/max_count;
		if(sm_height > max_height){
			sm_height = max_height;
			gap = (plot_rect.height - _top_margin - _bottom_margin - sm_height*max_count)/(max_count+1);
		}
		int sm_width = round(plot_rect.width * 0.4f);
		int label_width = round(plot_rect.width * 0.3f);

		Rectangle[] rects = new Rectangle[max_count];
		Gene[] genes = new Gene[max_count];
		Disease[] diseases = new Disease[max_count];

		int runningX = plot_rect.x + label_width;
		int runningY = plot_rect.y + gap;
		rectMode(CORNER);
		for(int i = 0; i < max_count; i++){
			//rectangle
			rects[i] = new Rectangle(runningX, runningY, sm_width, sm_height);
			//
			if(g_count == 1){
				genes[i] = display_gene_array.get(0);
			}else{
				if(i < display_gene_array.size()){
					genes[i] = display_gene_array.get(i);
				}
			}
			if(d_count == 1){
				diseases[i] = display_disease_array.get(0);
			}else{
				if(i < display_disease_array.size()){
					diseases[i] = display_disease_array.get(i);
				}
			}
			runningY += sm_height + gap;
		}	
		//draw small multiples
		for(int i = 0; i < max_count; i++){
			Rectangle r = rects[i];
			Gene g = genes[i];
			Disease d = diseases[i];

			float radian_increment = TWO_PI/(float)source_count;
			float start_radian = - PI/2;
			float end_radian = TWO_PI - PI/2;

			float center_x = (float)r.getCenterX();
			float center_y = (float)r.getCenterY();

			float running_radian = start_radian;
			float radius = r.height/2;
			//draw background
			// float runningY2 = r.y;
			//per data source
			for(int j = 0; j < source_count; j++){
				//outline
				stroke(240);
				strokeWeight(1);
				noFill();

				float dx = cos(running_radian) * radius + center_x;
				float dy = sin(running_radian) * radius + center_y;

				// line(center_x, center_y, dx, dy);

				draw_petal(center_x, center_y, dx, dy,radius, running_radian, 20);

				if(g!= null && d!= null){
					Link l = g.getLink(d, source_array.get(j));
					if(l == null){
						//dont draw
					}else{
						noStroke();
						fill(color_qualitative[j], 120);
						draw_petal(center_x, center_y, dx, dy,radius, running_radian, l.value);
						//draw value
						fill(120);
						textAlign(CENTER, CENTER);
						textFont(font);
						text(l.value, dx, dy);
					}						
				}
				running_radian += radian_increment;
			}

			//debug
			// stroke(220);
			// strokeWeight(1);
			// noFill();
			// rect(r.x, r.y, r.width, r.height);


			//text
			textFont(font2);
			fill(0);
			textAlign(CENTER, CENTER);
			text(g.name, plot_rect.x, r.y, label_width, r.height);
			text(d.name, r.x+r.width, r.y, label_width, r.height);
		}
	}
}

public void draw_petal(float cx, float cy, float dx, float dy, float radius, float radian, int value){
	float petal_ratio = 0.8f;
	float petal_peak = radius*petal_ratio;
	float petal_short = radius*(1-petal_ratio);
	float p_h = map(value, 0, 20, 1, (radius - petal_peak));
	float p_l = sqrt((petal_peak*petal_peak)+(p_h*p_h));

	float x3 = cos(radian) * petal_peak + cx;
	float y3 = sin(radian) * petal_peak + cy;

	float theta1 = atan2(p_h, petal_peak) +radian;
	float x1 = cos(theta1) * p_l + cx;
	float y1 = sin(theta1) * p_l + cy;

	float theta2 = radian - atan2(p_h, petal_peak);
	float x2 = cos(theta2) * p_l + cx;
	float y2 = sin(theta2) * p_l + cy;

	// float cr = sqrt((petal_short*petal_short)*2);
	float cr = sqrt((p_h*p_h)*2);
	float cx1 = cos(-PI/4 + radian) * cr + x3;
	float cy1 = sin(-PI/4 + radian) * cr + y3;
	float cx2 = cos(PI/4 + radian) * cr + x3;
	float cy2 = sin(PI/4 + radian) * cr + y3;
	float cx3 = cos(PI/4*3 + radian) * cr + x3;
	float cy3 = sin(PI/4*3 + radian) * cr + y3;
	float cx4 = cos(PI/4*5 + radian) * cr + x3;
	float cy4 = sin(PI/4*5 + radian) * cr + y3;


	beginShape();
	vertex(cx, cy);
	bezierVertex(cx, cy, cx4, cy4, x2, y2);
	bezierVertex(cx1, cy1, dx, dy, dx, dy);
	bezierVertex(dx, dy, cx2, cy2, x1, y1);
	bezierVertex(cx3, cy3, cx, cy, cx, cy);
	// bezierVertex()
	// vertex(dx, dy);
	// bezierVertex(cx2, cy2, cx3, cy3, x2, y2);
	// vertex(cx, cy);
	endShape();

	//debug
	// stroke(0);
	// strokeWeight(3);
	// strokeCap(ROUND);
	// point(x3, y3);
	// point(x1, y1);
	// point(x2, y2);

	// stroke(120);
	// point(cx1, cy1);
	// point(cx2, cy2);
	// point(cx3, cy3);
	// point(cx4, cy4);
}

public void plot_2_backup(){
	if(!selected_gene.equals("") || !selected_disease.equals("")){
		int gap = _margin*2;
		int max_height = _margin * 20;

		int g_count = display_gene_array.size();
		int d_count = display_disease_array.size();
		int source_count = source_array.size();
		int max_count = max(g_count, d_count);

		//determine small multiple size
		int sm_height = (plot_rect.height - _top_margin - _bottom_margin - gap*(max_count+1))/max_count;
		if(sm_height > max_height){
			sm_height = max_height;
			gap = (plot_rect.height - _top_margin - _bottom_margin - sm_height*max_count)/(max_count+1);
		}
		int sm_width = round(plot_rect.width * 0.4f);
		int label_width = round(plot_rect.width * 0.3f);

		Rectangle[] rects = new Rectangle[max_count];
		Gene[] genes = new Gene[max_count];
		Disease[] diseases = new Disease[max_count];

		int runningX = plot_rect.x + label_width;
		int runningY = plot_rect.y + gap;
		rectMode(CORNER);
		for(int i = 0; i < max_count; i++){
			//rectangle
			rects[i] = new Rectangle(runningX, runningY, sm_width, sm_height);
			//
			if(g_count == 1){
				genes[i] = display_gene_array.get(0);
			}else{
				if(i < display_gene_array.size()){
					genes[i] = display_gene_array.get(i);
				}
			}
			if(d_count == 1){
				diseases[i] = display_disease_array.get(0);
			}else{
				if(i < display_disease_array.size()){
					diseases[i] = display_disease_array.get(i);
				}
			}
			runningY += sm_height + gap;
		}	
		//draw small multiples
		for(int i = 0; i < max_count; i++){
			Rectangle r = rects[i];
			Gene g = genes[i];
			Disease d = diseases[i];

			float radian_increment = TWO_PI/(float)source_count;
			float start_radian = - PI/2;
			float end_radian = TWO_PI - PI/2;

			float center_x = (float)r.getCenterX();
			float center_y = (float)r.getCenterY();

			float running_radian = start_radian;
			float radius = r.height/2;
			//draw background
			// float runningY2 = r.y;
			//per data source
			for(int j = 0; j < source_count; j++){
				//outline
				stroke(240);
				strokeWeight(1);
				noFill();

				float dx = cos(running_radian) * radius + center_x;
				float dy = sin(running_radian) * radius + center_y;

				// line(center_x, center_y, dx, dy);

				draw_petal_2_backup(center_x, center_y, dx, dy,radius, running_radian, 20, false);

				if(g!= null && d!= null){
					Link l = g.getLink(d, source_array.get(j));
					if(l == null){
						//dont draw
					}else{
						noStroke();
						fill(color_qualitative[j], 120);
						draw_petal_2_backup(center_x, center_y, dx, dy,radius, running_radian, l.value, true);
					}						
				}
				running_radian += radian_increment;
			}

			//debug
			// stroke(220);
			// strokeWeight(1);
			// noFill();
			// rect(r.x, r.y, r.width, r.height);
			//text
			textFont(font2);
			fill(0);
			textAlign(CENTER, CENTER);
			text(g.name, plot_rect.x, r.y, label_width, r.height);
			text(d.name, r.x+r.width, r.y, label_width, r.height);
		}
	}
}

public void plot_2(){
	if(!selected_gene.equals("") || !selected_disease.equals("")){
		int gap = _margin*2;
		int max_height = _margin * 20;

		int g_count = display_gene_array.size();
		int d_count = display_disease_array.size();
		int source_count = source_array.size();
		int max_count = max(g_count, d_count);

		//determine small multiple size
		int sm_height = (plot_rect.height - _top_margin - _bottom_margin - gap*(max_count+1))/max_count;
		if(sm_height > max_height){
			sm_height = max_height;
			gap = (plot_rect.height - _top_margin - _bottom_margin - sm_height*max_count)/(max_count+1);
		}
		int sm_width = round(plot_rect.width * 0.4f);
		int label_width = round(plot_rect.width * 0.3f);

		Rectangle[] rects = new Rectangle[max_count];
		Gene[] genes = new Gene[max_count];
		Disease[] diseases = new Disease[max_count];

		int runningX = plot_rect.x + label_width;
		int runningY = plot_rect.y + gap;
		rectMode(CORNER);

		for(int i = 0; i < max_count; i++){
			//rectangle
			rects[i] = new Rectangle(runningX, runningY, sm_width, sm_height);
			//
			if(g_count == 1){
				genes[i] = display_gene_array.get(0);
			}else{
				if(i < display_gene_array.size()){
					genes[i] = display_gene_array.get(i);
				}
			}
			if(d_count == 1){
				diseases[i] = display_disease_array.get(0);
			}else{
				if(i < display_disease_array.size()){
					diseases[i] = display_disease_array.get(i);
				}
			}
			runningY += sm_height + gap;
		}	
		//draw small multiples
		for(int i = 0; i < max_count; i++){
			Rectangle r = rects[i];
			Gene g = genes[i];
			Disease d = diseases[i];

			float radian_increment = TWO_PI/(float)source_count;
			float start_radian = - PI/2;
			float end_radian = TWO_PI - PI/2;

			float center_x = (float)r.getCenterX();
			float center_y = (float)r.getCenterY();

			float running_radian = start_radian;
			float radius = r.height/2;
			//draw background
			// float runningY2 = r.y;
			//per data source


			float max_area = getPetalArea(center_x, center_y, radius, running_radian, running_radian+radian_increment);
			float min_area = getPetalArea(center_x, center_y, _margin*2, running_radian, running_radian+radian_increment);



			for(int j = 0; j < source_count; j++){
				//outline
				stroke(240);
				strokeWeight(1);
				noFill();

				float dx = cos(running_radian +radian_increment/2) * radius + center_x;
				float dy = sin(running_radian +radian_increment/2) * radius + center_y;

				// line(center_x, center_y, dx, dy);
				// draw_petal_2(center_x, center_y, dx, dy,radius, running_radian, radian_increment, 20, false);
				drawPetal(center_x, center_y, radius, running_radian, running_radian+radian_increment);

				if(g!= null && d!= null){
					Link l = g.getLink(d, source_array.get(j));
					if(l == null){
						//dont draw
					}else{
						noStroke();
						fill(color_qualitative[j], 120);
						float mapped_area = map(l.value, 1, 20, min_area, max_area);
						float mapped_r = getRadius(mapped_area, center_x, center_y, running_radian, running_radian+radian_increment);
						drawPetal(center_x, center_y, mapped_r, running_radian, running_radian+radian_increment);
						// draw_petal_2(center_x, center_y, dx, dy,radius, running_radian, radian_increment, l.value, true);

						fill(120);
						textAlign(CENTER, CENTER);
						textFont(font);
						text(""+l.value, dx, dy);
					}						
				}
				running_radian += radian_increment;
			}

			//debug
			// stroke(220);
			// strokeWeight(1);
			// noFill();
			// rect(r.x, r.y, r.width, r.height);
			//text
			textFont(font2);
			fill(0);
			textAlign(CENTER, CENTER);
			text(g.name, plot_rect.x, r.y, label_width, r.height);
			text(d.name, r.x+r.width, r.y, label_width, r.height);
		}
	}
}
public void draw_petal_2(float cx, float cy, float dx, float dy, float radius, float radian, float theta, int value, boolean showValue){
	float k = 0.55228f;
	float a = radius/(tan(theta/2)+1);
	float r = radius - a;

	float x4 = cos(radian) * a + cx;
	float y4 = sin(radian) * a + cy;

	float x1 = cos(radian - PI/2) * r + x4;
	float y1 = sin(radian - PI/2) * r + y4;
	float x3 = cos(radian + PI/2) * r + x4;
	float y3 = sin(radian + PI/2) * r + y4;

	//control
	float b = r*k;
	float c = sqrt((r*r)+(b+b));
	float theta2 = atan2(b, r);

	// println("b="+b+ " c="+c+ " degrees="+degrees(theta2)+" r="+r);
	float c2x = cos(radian - theta2) * c + x4;
	float c2y = sin(radian - theta2) * c + y4;
	float c1x = cos(radian - PI/2 +theta2) * c + x4;
	float c1y = sin(radian - PI/2 +theta2) * c + y4;

	float c3x = cos(radian + theta2) * c + x4;
	float c3y = sin(radian + theta2) * c + y4;
	float c4x = cos(radian + PI/2 -theta2) * c + x4;
	float c4y = sin(radian + PI/2 -theta2) * c + y4;

	float[] s1 = bezierVertexArc(x4, y4, r, radian-PI/2, radian);
	float[] s2 = bezierVertexArc(x4, y4, r, radian, radian+PI/2 );
	
	beginShape();
	vertex(cx, cy);
	vertex(s1[0], s1[1]);


	vertex(s2[0], s2[0]);

	// vertex(x1, y1);
	// bezierVertex(c1x, c1y, c2x, c2y, dx, dy);
	// bezierVertex(c3x, c3y, c4x, c4y, x3, y3);
	vertex(cx, cy);
	endShape();


	//debug
	// stroke(200);
	// strokeWeight(1);
	// noFill();
	// line(cx, cy, dx, dy);
	// line(cx, cy, x1, y1);
	// line(cx, cy, x3, y3);
	// ellipse(x4, y4, r*2, r*2);

	// stroke(120);
	// strokeWeight(3);
	// strokeCap(ROUND);
	// // point(x4, y4);
	// // point(x1, y1);

	// stroke(0);
	// point(c2x, c2y);
	// point(c1x, c1y);

	// strokeWeight(1);
}

public void draw_petal_2_backup(float cx, float cy, float dx, float dy, float radius, float radian, int value, boolean showValue){
	float max_r = radius *(1/(1+sqrt(3)));
	float max_area = 2*max_r* max_r*((1/3)*PI + 1);
	float max_value = 20;// the largest value for petal
	float min_area = max_area/max_value;	

	//map value to area
	float mapped_area = map(value, 1, max_value, min_area, max_area);
	float mapped_r = sqrt(mapped_area/((2/3)*PI + 2));

	// println("max_r ="+max_r+"  mapped_r="+mapped_r);

	float r = mapped_r*(1+sqrt(3));
	float x1 = cos(radian) * r +cx;
	float y1 = sin(radian) * r +cy;
	float r2 = r/(1+sqrt(3));
	float x2 = cos(radian+PI/6) * (2*r2) + cx;
	float y2 = sin(radian+PI/6) * (2*r2) + cy;
	float x3 = cos(radian-PI/6) * (2*r2) + cx;
	float y3 = sin(radian-PI/6) * (2*r2) + cy;

	float r3 = r - r2;
	float x4 = cos(radian) * (r3) + cx;
	float y4 = sin(radian) * (r3) + cy;

	float cx1 = cos(radian + PI/2) * r2 + x1;
	float cy1 = sin(radian + PI/2) * r2 + y1;
	float cx4 = cos(radian - PI/2) * r2 + x1;
	float cy4 = sin(radian - PI/2) * r2 + y1;

	// float cx1 = cos(radian + PI/4) * r2 + x4;
	// float cy1 = sin(radian + PI/4) * r2 + y4;
	// float cx4 = cos(radian - PI/4) * r2 + x4;
	// float cy4 = sin(radian - PI/4) * r2 + y4;

	// float cx2 = cos(radian + PI/4*3) * r2 + x4;
	// float cy2 = sin(radian + PI/4*3) * r2 + y4;
	// float cx3 = cos(radian - PI/4*3) * r2 + x4;
	// float cy3 = sin(radian - PI/4*3)* r2 + y4;


	// float cx3 = cos(PI/4*3 + radian) * cr + x3;
	// float cy3 = sin(PI/4*3 + radian) * cr + y3;
	// float cx4 = cos(PI/4*5 + radian) * cr + x3;
	// float cy4 = sin(PI/4*5 + radian) * cr + y3;


	beginShape();
	vertex(cx, cy);
	vertex(x2, y2);
	quadraticVertex(cx1, cy1, x1, y1);
	quadraticVertex(cx4, cy4, x3, y3);
	// bezierVertexArc(x4, y4, r3, (radian - (PI+radian)/2), (radian + (PI+radian)/2));

	vertex(cx, cy);

	// vertex(x2, y2);

	// vertex(x1, y1);
	// vertex(x3, y3);
	// vertex(cx, cy);

	// vertex(cx, cy);
	// curveVertex(cx, cy);
	// curveVertex(x2, y2);
	// curveVertex(x1, y1);
	// curveVertex(x3, y3);
	// curveVertex(cx, cy);
	// vertex(x3, y3);
	// vertex(cx, cy);



	// bezierVertex(cx, cy, cx2, cy2, x2, y2);
	// bezierVertex(cx1, cy1, cx1, cy1, x1, y1);
	// bezierVertex(cx4, cy4, cx4, cy4, x3, y3);
	// bezierVertex(cx3, cy3, cx, cy, cx, cy);
	// vertex(cx, cy);

	// vertex(cx, cy);
	// bezierVertex(cx, cy, cx4, cy4, x2, y2);
	// bezierVertex(cx1, cy1, dx, dy, dx, dy);
	// bezierVertex(dx, dy, cx2, cy2, x1, y1);
	// bezierVertex(cx3, cy3, cx, cy, cx, cy);
	endShape();

	//draw value
	if(showValue){
		fill(120);
		textAlign(CENTER, CENTER);
		textFont(font);
		text(value, x1, y1);
	}

	//debug
	stroke(200);
	strokeWeight(1);
	noFill();
	// line(cx, cy, x1, y1);
	// line(x4, y4, x2, y2);
	// line(x4, y4, x3, y3);
	// ellipseMode(CENTER);
	// ellipse(x4, y4, r2*2, r2*2);



	stroke(0);
	strokeWeight(3);
	strokeCap(ROUND);

	// point(cx1, cy1);
	// point(x4, y4);
	// point(cx1, cy1);
	// point(cx4, cy4);
	// point(dx, dy);
	// point(cx, cy);
	// point(x2, y2);

	stroke(120);
	// point(x1, y1);
	// point(x2, y2);
	// point(x3, y3);
	// point(cx1, cy1);
	// point(cx2, cy2);
	// point(cx3, cy3);
	// point(cx4, cy4);

	strokeWeight(1);
}

public void drawPetal(float x, float y, float r, float start, float end){
	float cx = x;
	float cy = y;
	float theta = abs(start - end);

	float x2 = cos(start + theta/2) * r + cx;
	float y2 = sin(start + theta/2) * r + cy;	

	float s = r /(sin(theta/2)+1);
	float r2 = r - s;

	float x4 = cos(start + theta/2) * s + cx;
	float y4 = sin(start + theta/2) * s + cy;

	float l = sqrt(r2*r2 + s*s);
	float x1 = cos(start) * l + cx;
	float y1 = sin(start) * l + cy;
	float x3 = cos(end) * l + cx;
	float y3 = sin(end) * l + cy;

	//control points
	float kappa = 0.55228f;
	float ch = r2 * kappa;
	float co = sqrt(ch*ch + r2*r2);
	float t2 = atan2(ch, r2);

	// println("s="+s+"  r2="+r2+" ch="+ch+ " co="+co+ " agnle="+degrees(t2));

	// float cx1 = cos(theta/2 - PI/2 - theta/2) * co + x4;
	// float cy1 = sin(theta/2 - PI/2 - theta/2) * co + y4;
	float cx1 = cos(t2+start + theta/2 - PI/2) * co + x4;
	float cy1 = sin(t2+start + theta/2 - PI/2) * co + y4;
	float cx4 = cos(-1*t2 +start + theta/2 + PI/2) * co + x4;
	float cy4 = sin(-1*t2 +start + theta/2 + PI/2) * co + y4;
	float cx2 = cos(-1*t2 +start + theta/2) * co + x4;
	float cy2 = sin(-1*t2 +start + theta/2) * co + y4;
	float cx3 = cos(t2+start + theta/2) * co + x4;
	float cy3 = sin(t2+start + theta/2) * co + y4;

	float cx0 = cos(-1*t2+ start + theta/2 - PI/2) * co + x4;
	float cy0 = sin(-1*t2+ start + theta/2 - PI/2) * co + y4;
	float cx5 = cos(t2 +start + theta/2 + PI/2) * co + x4;
	float cy5 = sin(t2 +start + theta/2 + PI/2) * co + y4;

	beginShape();
	vertex(cx, cy);
	vertex(x1, y1);
	// bezierVertex(cx, cy, cx0, cy0, x1, y1);
	bezierVertex(cx1, cy1, cx2, cy2, x2, y2);
	bezierVertex(cx3, cy3, cx4, cy4, x3, y3);
	// bezierVertex(cx5, cy5, cx, cy, cx, cy);
	vertex(cx, cy);

	endShape();
}
//approximate the area
public float getPetalArea(float x, float y, float r, float start, float end){
	float cx = x;
	float cy = y;
	float theta = abs(start - end);

	float x2 = cos(start + theta/2) * r + cx;
	float y2 = sin(start + theta/2) * r + cy;	

	float s = r /(sin(theta/2)+1);
	float r2 = r - s;

	float result = (r2*r2*PI)/2 + r2*s;
	return result;
}	
public float getRadius(float area, float x, float y, float start, float end){
	float cx = x;
	float cy = y;
	float theta = abs(start - end);

	float r2 = sqrt((2*area*theta)/(4+PI*theta));
	float s = (2*r2)/theta;

	float r = r2 + s;
	return r;
}


public void plot_3(){
	if(!selected_gene.equals("") || !selected_disease.equals("")){
		int gap = _margin*2;
		int max_height = _margin * 20; // height of small multiple

		int g_count = display_gene_array.size();
		int d_count = display_disease_array.size();
		int source_count = source_array.size();
		int max_count = max(g_count, d_count);

		//determine small multiple size
		int sm_height = (plot_rect.height - _top_margin - _bottom_margin - gap*(max_count+1))/max_count;
		if(sm_height > max_height){
			sm_height = max_height;
			gap = (plot_rect.height - _top_margin - _bottom_margin - sm_height*max_count)/(max_count+1);
		}
		int sm_width = round(plot_rect.width * 0.4f);
		int label_width = round(plot_rect.width * 0.3f);

		Rectangle[] rects = new Rectangle[max_count];
		Gene[] genes = new Gene[max_count];
		Disease[] diseases = new Disease[max_count];

		int runningX = plot_rect.x + label_width;
		int runningY = plot_rect.y + gap;
		rectMode(CORNER);
		for(int i = 0; i < max_count; i++){
			//rectangle
			rects[i] = new Rectangle(runningX, runningY, sm_width, sm_height);
			//
			if(g_count == 1){
				genes[i] = display_gene_array.get(0);
			}else{
				if(i < display_gene_array.size()){
					genes[i] = display_gene_array.get(i);
				}
			}
			if(d_count == 1){
				diseases[i] = display_disease_array.get(0);
			}else{
				if(i < display_disease_array.size()){
					diseases[i] = display_disease_array.get(i);
				}
			}
			runningY += sm_height + gap;
		}	
		//node size
		float node_gap_total = (sm_height*0.3f); //size of gap > size of bar height
		float node_gap = node_gap_total/(float)(source_count-1);
		//max line width
		float node_height = (sm_height - node_gap_total)/ (float)source_count;

		//draw small multiples
		int max_link_value = 20; //change this for scaling
		for(int i = 0; i < max_count; i++){
			Rectangle r = rects[i];
			Gene g = genes[i];
			Disease d = diseases[i];
			//draw background
			float runningY2 = r.y;
			//left
			float x1 = r.x ;
			float y1 = (float) r.getCenterY();
			//right
			float x2 = r.x+r.width ;
			float y2 = y1;

			//left and right outline
			stroke(240);
			strokeWeight(1);
			noFill();
			ellipse(x1, y1, node_height, node_height);
			ellipse(x2, y2, node_height, node_height);

			//center
			float cx = (float) r.getCenterX();
			int min_weight = 4;

			//control
			float cx2 = cx - (r.width/6);
			float cx3 = cx + (r.width/6);

			//per data source
			for(int j = 0; j < source_count; j++){
				//outline
				stroke(240);
				strokeWeight(1);
				noFill();
				// rect(r.x, runningY2, r.width, node_height);

				ellipseMode(CENTER);
				ellipse(cx, runningY2+node_height/2, node_height, node_height);


				if(g!= null && d!= null){
					Link l = g.getLink(d, source_array.get(j));
					if(l == null){
						//dont draw
					}else{
						float stroke_weight = map(l.value, 1, max_link_value, min_weight, node_height);
						noFill();
						stroke(color_qualitative[j]);
						strokeWeight(stroke_weight);
						strokeCap(ROUND);
						strokeJoin(ROUND);
						beginShape();
						vertex(x1, y1);
						// vertex(cx, runningY2+node_height/2);
						// vertex(x2, y2);
						bezierVertex(x1, y1, cx2, runningY2+node_height/2 , cx, runningY2+node_height/2);
						bezierVertex(cx3, runningY2+node_height/2, x2, y2, x2, y2);
						endShape();

						//ellipse
						// strokeWeight(1);
						// stroke(60);
						// fill(255);
						// ellipse(cx, runningY2+node_height/2, stroke_weight, stroke_weight);

						//draw value
						fill(200);
						textAlign(CENTER, TOP);
						textFont(font);
						text(l.value, cx, runningY2+node_height);
					}						
				}
				runningY2 += node_height + node_gap;
			}

			// strokeWeight(10);
			// stroke(200);
			// strokeCap(ROUND);
			// point(x1, y1);
			// point(x2, y2);
			strokeWeight(1);
			// noFill();
			// rect(r.x, r.y, r.width, r.height);


			//text
			textFont(font2);
			fill(0);
			textAlign(RIGHT, CENTER);
			text(g.name, plot_rect.x, r.y, label_width-_margin, r.height);
			textAlign(LEFT, CENTER);
			text(d.name, r.x+r.width+_margin, r.y, label_width-_margin, r.height);
		}
	}
}

public void plot_4(){
	if(!selected_gene.equals("") || !selected_disease.equals("")){
		int gap = _margin*2;
		int max_height = _margin * 20; // height of small multiple

		int g_count = display_gene_array.size();
		int d_count = display_disease_array.size();
		int source_count = source_array.size();
		int max_count = max(g_count, d_count);

		//determine small multiple size
		int sm_height = (plot_rect.height - _top_margin - _bottom_margin - gap*(max_count+1))/max_count;
		if(sm_height > max_height){
			sm_height = max_height;
			gap = (plot_rect.height - _top_margin - _bottom_margin - sm_height*max_count)/(max_count+1);
		}
		int sm_width = round(plot_rect.width * 0.4f);
		int label_width = round(plot_rect.width * 0.3f);

		Rectangle[] rects = new Rectangle[max_count];
		Gene[] genes = new Gene[max_count];
		Disease[] diseases = new Disease[max_count];

		int runningX = plot_rect.x + label_width;
		int runningY = plot_rect.y + gap;
		rectMode(CORNER);
		for(int i = 0; i < max_count; i++){
			//rectangle
			rects[i] = new Rectangle(runningX, runningY, sm_width, sm_height);
			//
			if(g_count == 1){
				genes[i] = display_gene_array.get(0);
			}else{
				if(i < display_gene_array.size()){
					genes[i] = display_gene_array.get(i);
				}
			}
			if(d_count == 1){
				diseases[i] = display_disease_array.get(0);
			}else{
				if(i < display_disease_array.size()){
					diseases[i] = display_disease_array.get(i);
				}
			}
			runningY += sm_height + gap;
		}	
	
		//draw small multiples
		for(int i = 0; i < max_count; i++){
			Rectangle r = rects[i];
			Gene g = genes[i];
			Disease d = diseases[i];

			float radian_increment = TWO_PI/(float)source_count;
			float start_radian = - PI/2;
			float end_radian = TWO_PI - PI/2;

			float center_x = (float)r.getCenterX();
			float center_y = (float)r.getCenterY();
			float center_r = _margin*4;

			float running_radian = start_radian;
			float radius = r.height/2;

			//find area
			float max_area = radius*radius*PI/(float)source_count;
			float min_area = center_r*center_r*PI/(float)source_count;
			float min_r = sqrt(min_area/PI);

			//draw background
			stroke(240);
			strokeWeight(1);
			noFill();
			ellipse(center_x, center_y, radius*2, radius*2);
			ellipse(center_x, center_y, min_r*2, min_r*2);

			int max_link_value = 20;

			//per data source
			for(int j = 0; j < source_count; j++){
				//outline
				stroke(240);
				strokeWeight(1);
				noFill();

				float dx = cos(running_radian) * radius + center_x;
				float dy = sin(running_radian) * radius + center_y;

				line(center_x, center_y, dx, dy);

				if(g!= null && d!= null){
					Link l = g.getLink(d, source_array.get(j));
					if(l == null){
						//dont draw
					}else{
						//map area
						float mapped_area = map(l.value, 0, max_link_value, min_area, max_area);
						float mapped_r = sqrt(5*mapped_area/PI);
						float w = mapped_r*2;
						float h = w;
						float start = running_radian;
						float end = start + radian_increment;

						noStroke();
						fill(color_qualitative[j], 120);
						arc(center_x, center_y, w, h, start, end, PIE);

						//label
						float lx = cos(running_radian+(radian_increment/2)) * (mapped_r+_margin) + center_x;
						float ly = sin(running_radian+(radian_increment/2)) * (mapped_r+_margin) + center_y;
						textAlign(CENTER, CENTER);
						fill(120);
						textFont(font);
						text(l.value, lx, ly);
					}						
				}
				running_radian += radian_increment;
			}

			//white center
			stroke(240);
			fill(255);
			ellipse(center_x, center_y, min_r*2, min_r*2);

			textFont(font2);
			fill(0);
			textAlign(CENTER, CENTER);
			text(g.name, plot_rect.x, r.y, label_width, r.height);
			text(d.name, r.x+r.width, r.y, label_width, r.height);
		}
	}
}


public float[] bezierVertexArc(float x, float y, float r, float start, float end){
	float px0,py0;
	float px1,py1;
	float px2,py2;
	float px3,py3;
	 
	float theta = abs(start - end); // spread of the arc.

	// Compute raw Bezier coordinates.
	float x0 = cos(theta/2.0f);
	float y0 = sin(theta/2.0f);
	float x3 = x0;
	float y3 = 0-y0;
	float x1 = (4.0f-x0)/3.0f;
	float y1 = ((1.0f-x0)*(3.0f-x0))/(3.0f*y0); // y0 != 0...
	float x2 = x1;
	float y2 = 0-y1;

	// Compute rotationally-offset Bezier coordinates, using:
	// x' = cos(angle) * x - sin(angle) * y;
	// y' = sin(angle) * x + cos(angle) * y;
	float bezAng = start + theta/2.0f;
	float cBezAng = cos(bezAng);
	float sBezAng = sin(bezAng);
	float rx0 = cBezAng * x0 - sBezAng * y0;
	float ry0 = sBezAng * x0 + cBezAng * y0;
	float rx1 = cBezAng * x1 - sBezAng * y1;
	float ry1 = sBezAng * x1 + cBezAng * y1;
	float rx2 = cBezAng * x2 - sBezAng * y2;
	float ry2 = sBezAng * x2 + cBezAng * y2;
	float rx3 = cBezAng * x3 - sBezAng * y3;
	float ry3 = sBezAng * x3 + cBezAng * y3;

	// Compute scaled and translated Bezier coordinates.
	px0 = x + r*rx0;
	py0 = y + r*ry0;
	px1 = x + r*rx1;
	py1 = y + r*ry1;
	px2 = x + r*rx2;
	py2 = y + r*ry2;
	px3 = x + r*rx3;
	py3 = y + r*ry3;

	// Draw the Bezier control points.
	// stroke(0,0,0, 64);
	// fill  (0,0,0, 64);
	// ellipse(px0,py0, 8,8);
	// ellipse(px1,py1, 8,8);
	// ellipse(px2,py2, 8,8);
	// ellipse(px3,py3, 8,8);
	// line (x,y,   px0,py0);
	// line (px0,py0, px1,py1);
	// line (px1,py1, px2,py2);
	// line (px2,py2, px3,py3);
	// line (px3,py3,   x,y);

	//--------------------------
	// BLUE IS THE "TRUE" CIRULAR ARC:
	// noFill();
	// stroke(0,0,180, 128);
	// arc(x, y, r*2, r*2, start, end);

	//--------------------------
	// RED IS THE BEZIER APPROXIMATION OF THE CIRCULAR ARC:
	// noFill();
	// stroke(255,0,0, 128);
	//////
	// bezier(px0,py0, px1,py1, px2,py2, px3,py3);
	// vertex(px0,py0);
	// bezierVertex(px1, py1, px2, py2, px3, py3);
	float[] result = {px0, py0,px1, py1,px2, py2,px3, py3};
	return result;
}




public void draw_btns(){
	textFont(font2);
	textAlign(CENTER, CENTER);
	noStroke();
	for(int i = 0; i < design_labels.length; i++){
		Rectangle r = design_btn_rects[i];
		if(i == design_index){
			fill(60);
			rect(r.x, r.y, r.width, r.height);
			fill(255);
			text(design_labels[i], (float)r.getCenterX(), (float)r.getCenterY());
		}else{
			fill(200);
			rect(r.x, r.y, r.width, r.height);
			fill(180);
			text(design_labels[i], (float)r.getCenterX(), (float)r.getCenterY());
		}

	}
	textFont(font);
}

public void mouseMoved(){
	cursor(ARROW);
	if(source_list.scroll_bar.contains(mouseX, mouseY)){
		cursor(HAND);
		loop();
		return;
	}else if(source_list.whole_rect.contains(mouseX, mouseY)){
		//list area
		if(source_list.checkMouseOver(mouseX, mouseY)){
			cursor(HAND);
			loop();
			return;
		}
	}else if(target_list.scroll_bar.contains(mouseX, mouseY)){
		cursor(HAND);
		loop();
		return;
	}else if(target_list.whole_rect.contains(mouseX, mouseY)){
		//list area
		if(target_list.checkMouseOver(mouseX, mouseY)){
			cursor(HAND);
			loop();
			return;
		}
	}else if(design_btn_area.contains(mouseX, mouseY)){
		for(Rectangle r:design_btn_rects){
			if(r.contains(mouseX, mouseY)){
				cursor(HAND);
				return;
			}
		}
	}
}

public void mousePressed(){
	// println("debug:click....");
	//Text list
	if(source_list.scroll_bar.contains(mouseX, mouseY)){
		source_list.isActive = true;
	}else if(source_list.whole_rect.contains(mouseX, mouseY)){
		selected_gene = source_list.hovering_item;
		source_list.selected_item = selected_gene;
		//update list
		update_display_array();
	}else if(target_list.scroll_bar.contains(mouseX, mouseY)){
		target_list.isActive = true;
	}else if(target_list.whole_rect.contains(mouseX, mouseY)){
		selected_disease = target_list.hovering_item;
		target_list.selected_item = selected_disease;
		// println("\tselected_disease="+ target_list.hovering_item);
		// println("\ttarget_list.selected_item="+ target_list.selected_item);
		// println("debug: click on:\'"+selected_disease+"\'");
		//update  list
		update_display_array();

	}
	// println("debug:selcted gene=\'"+selected_gene+"\'  disease =\'"+selected_disease+"\'");

	return;
}

public void mouseDragged(){
	if(source_list.isActive){
		source_list.updateList(mouseY);
		loop();
	}else if(target_list.isActive){
		target_list.updateList(mouseY);
		loop();
	}
}

public void mouseReleased(){
	source_list.isActive = false;
	target_list.isActive = false;
	loop();	
}

public void keyPressed(){
	if(key == '1'){
		this.design_index = 0;
	}else if(key == '2'){
		this.design_index = 1;
	}else if(key == '3'){
		this.design_index = 2;
	}else if(key == '4'){
		this.design_index = 3;
	}else if(key == '5'){
		this.design_index = 4;
	}
	loop();
}

class GeneAlphabetComparator implements Comparator<Gene> { 
	public int compare(Gene a, Gene b) { 
		return a.name.compareToIgnoreCase(b.name); 
	} 
}

class DiseaseAlphabetComparator implements Comparator<Disease> { 
	public int compare(Disease a, Disease b) { 
		return a.name.compareToIgnoreCase(b.name); 
	} 
}

class Disease{
	String url = "";
	String name = "";
	String top_level = ""; //name of top level
	boolean isRare = false;
	boolean isCommon = false;
	int group = -1;
	String id = "";
	boolean isEFO = false;
	boolean isOrphanet = false;
	boolean isTopLevel  = false;
	int index = -1;

	ArrayList<Link> links;

	Disease(String url, String name, String top_level, String type, int group, int index){
		this.url = url;
		this.name = name;
		this.top_level = top_level;
		this.group = group;
		this.index = index;

		String[] split = split(url, "/");
		this.id = split[split.length-1];

		if(type.equals("CD")){
			isCommon = true;
		}else if(type.equals("RD")){
			isRare = true;
		}
		if(this.id.startsWith("EFO")){
			this.isEFO = true;
		}else if(this.id.startsWith("Orphanet")){
			this.isOrphanet = true;
		}else{
			println("debug:Disease:unknown id:"+id);
		}

		if(this.name.equals(top_level)){
			isTopLevel = true;
		}

		links = new ArrayList<Link>();
	}

	public HashSet<Gene> getGeneArray(){
		HashSet<Gene> result = new HashSet<Gene>();
		for(Link l:links){
			result.add(l.gene);
		}
		return result;
	}



}
class DiseaseGroup{
	String name = "";
	ArrayList<Disease> array;


	DiseaseGroup(String name){
		this.name = name;
		this.array = new ArrayList<Disease>();
	}
}
class Gene{
	String name = "";
	int group = 0;
	String acc = ""; // "ENSG0000000000"
	int index = -1;

	ArrayList<Link> links;

	Gene(String name, String acc, int group, int index){
		this.name = name;
		this.acc = acc;
		this.group = group;
		this.index = index;
		links = new ArrayList<Link>();
	}

	public ArrayList<Link> getLinks(Disease d){
		ArrayList<Link> result = new ArrayList<Link>();
		for(Link l: links){
			if(l.disease == d){
				result.add(l);
			}
		}
		return result;
	}

	

	public HashSet<Disease> getDiseaseArray(){
		HashSet<Disease> result = new HashSet<Disease>();
		for(Link l:links){
			result.add(l.disease);
		}
		return result;
	}

	public Link getLink(Disease d, String source){
		Link result = null;
		for(Link l: links){
			if(l.disease == d && l.source.equals(source)){
				result = l;
				return result;
			}
		}
		return result;
	}
}
class Link{
	Gene gene;
	Disease disease;
	String source = "";
	int value = 0;
	String version = ""; //cttv000


	Link(Gene gene, Disease disease, String datasource, int value){
		this.gene = gene;
		this.disease = disease;
		this.value = value;
		String[] split = split(datasource, "_");
		this.version = split[0];
		this.source = split[1];
		if(split.length>2){
			for(int i = 2; i<split.length; i++){
				this.source += "_"+split[i];
			}
		}

	}
}
class TextList{
	ArrayList<String> list;
	int title_height = 15;
	int item_height = 15;
	int scroll_bar_width = 15;


	int item_text_highlight = 60;
	int item_text_color = 120;
	int list_bg_color = 255;
	int list_bg_outline = 180;
	int scroll_area_color = 230;
	int scroll_color = 120;

	Rectangle whole_rect, scroll_bar, scroll_area;

	int list_index = 0; //current position of index
	int list_index_min = 0;
	int list_index_max;

	int list_item_count = 0;

	boolean isActive = false;
	boolean onDrag = false;
	int onDrag_pos = 0;
	float onPress_pos = 0; //relative position

	String hovering_item = "";
	String selected_item = "";

	TextList(Rectangle area){
		list = new ArrayList<String>();
		int scroll_x = area.x + area.width - scroll_bar_width;
		int scroll_y = area.y;
		int scroll_w = scroll_bar_width;
		int scroll_h = area.height;
		list_item_count = round((float)scroll_h/ (float)item_height);
		scroll_h =  list_item_count * item_height;
		
		this.whole_rect = area;
		scroll_area = new Rectangle(scroll_x, scroll_y, scroll_w, scroll_h);
		scroll_bar = new Rectangle(scroll_x, scroll_y, scroll_w, scroll_h);
	}

	public void display(){
		rectMode(CORNER);
		// noStroke();
		// noFill();
		stroke(list_bg_outline);
		fill(list_bg_color);
		rect(whole_rect.x, whole_rect.y, whole_rect.width, whole_rect.height);
		//scroll scroll_area
		fill(scroll_area_color);
		rect(scroll_area.x, scroll_area.y, scroll_area.width, scroll_area.height);

		//scroll bar
		int total_item_count = list.size();
		// println("total item count = "+total_item_count);
		if(total_item_count > list_item_count){
			// int bar_height = round(scroll_area.height * (float)list_item_count/(float)total_item_count);
			int bar_y = round(map(list_index, 0, total_item_count, scroll_area.y, scroll_area.y+scroll_area.height));
			scroll_bar.setLocation(scroll_area.x, bar_y);
			noStroke();
			fill(scroll_color);	
			//rounded rect
			rect(scroll_bar.x, scroll_bar.y, scroll_bar.width, scroll_bar.height, 5, 5, 5, 5);
		}
		
		//draw items
		textAlign(LEFT, CENTER);
		float runningY = scroll_area.y + (float)item_height/2;
		float runningX = whole_rect.x;
		
		for(int i = list_index; i < list_index+list_item_count; i++){
			if(i < list.size()){
				fill(item_text_color);
				if(hovering_item.equals(list.get(i))){
					fill(item_text_highlight);
				}
				//when selected
				if(selected_item.equals(list.get(i))){
					fill(color_red);
				}
				text("  "+list.get(i), runningX, runningY);
				runningY+= item_height;
			}
		}
	}

	//call after adding all the items
	public void update(){
		int item_count = list.size();
		list_index = 0;
		list_index_min = 0;
		if(item_count > list_item_count){
			list_index_max = item_count - list_item_count;
			int bar_height = round(scroll_area.height * (float)list_item_count/(float)item_count);
			bar_height = max(bar_height, 5);

			int bar_y = round(map(list_index, 0, item_count-1, scroll_area.y, scroll_area.y+scroll_area.height));
			scroll_bar = new Rectangle(scroll_area.x, bar_y, scroll_bar_width, bar_height);

		}else{
			list_index_max = 1;
		}
	}

	//update index based on registerd onDrag_pos in relation to m_y
	public void updateList(int m_y){
		//determine tip of bar
		int tip_y = round(constrain(m_y - onPress_pos*scroll_bar.height, scroll_area.y, scroll_area.y+scroll_area.height- scroll_bar.height));
		
		//updeate index
		list_index = round(map(tip_y, scroll_area.y, scroll_area.y + scroll_area.height, 0, list.size()));
		list_index = constrain(list_index, 0, list.size()-1);
	}


	public void addItem(String name){
		this.list.add(name);
	}

	public void reset(){
		list = new ArrayList<String>();
		hovering_item = "";
		selected_item = "";
	}

	public boolean checkMouseOver(int mx, int my){
		int n =  floor((my - whole_rect.y)/item_height) + list_index;
		if(n < list.size()){
			//node list
			String label = list.get(n);
			// println("debug label = "+label);
			hovering_item = label;
			return true;
		}else{
			hovering_item = "";
		}
		return false;
	}

}
class TextListGene extends TextList{
	TextListGene(Rectangle area){
		super(area);
	}

	public void display(){
		rectMode(CORNER);
		stroke(list_bg_outline);
		fill(list_bg_color);
		rect(whole_rect.x, whole_rect.y, whole_rect.width, whole_rect.height);
		//scroll scroll_area
		fill(scroll_area_color);
		rect(scroll_area.x, scroll_area.y, scroll_area.width, scroll_area.height);

		//scroll bar
		int total_item_count = list.size();
		// println("total item count = "+total_item_count);
		if(total_item_count > list_item_count){
			// int bar_height = round(scroll_area.height * (float)list_item_count/(float)total_item_count);
			int bar_y = round(map(list_index, 0, total_item_count, scroll_area.y, scroll_area.y+scroll_area.height));
			scroll_bar.setLocation(scroll_area.x, bar_y);
			noStroke();
			fill(scroll_color);	
			//rounded rect
			rect(scroll_bar.x, scroll_bar.y, scroll_bar.width, scroll_bar.height, 5, 5, 5, 5);
		}
		
		//draw items
		textAlign(LEFT, CENTER);
		float runningY = scroll_area.y + (float)item_height/2;
		float runningX = whole_rect.x;
		
		strokeCap(SQUARE);
		for(int i = list_index; i < list_index+list_item_count; i++){
			if(i < list.size()){
				fill(item_text_color);
				if(hovering_item.equals(list.get(i))){
					fill(item_text_highlight);
				}
				//when selected
				if(selected_item.equals(list.get(i))){
					fill(color_red);
				}
				text("  "+list.get(i), runningX, runningY);

				//draw Link type
				Gene g = display_gene_array.get(i);
				float content_x = scroll_area.x - _margin;
				for(Link l : g.links){
					strokeWeight(2);
					stroke(color_qualitative[source_array.indexOf(l.source)]);
					line(content_x, runningY, content_x, runningY+item_height);
					content_x -= 3;
				}
				runningY+= item_height;
			}
		}
		strokeWeight(1);
	}
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "GraphPrototype" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
