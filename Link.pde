class Link{
	Gene gene;
	Disease disease;
	String source = "";
	int value = 0;
	String version = ""; //cttv000


	Link(Gene gene, Disease disease, String datasource, int value){
		this.gene = gene;
		this.disease = disease;
		this.value = value;
		String[] split = split(datasource, "_");
		this.version = split[0];
		this.source = split[1];
		if(split.length>2){
			for(int i = 2; i<split.length; i++){
				this.source += "_"+split[i];
			}
		}

	}
}